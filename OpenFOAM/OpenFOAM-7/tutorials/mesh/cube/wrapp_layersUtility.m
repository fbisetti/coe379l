clear all
close all
clc

% h0 is the base cell size
% h4, h5, and h6 are sizes at level 4,5, and 6
% d1 and thick are the desired first layer thickness and overall thicknes
% nlayers is the number of layers to be added

% thin layer

h4=1/2^4;
h5=1/2^5;
h6=1/2^6;

d1=1.67e-4;
thick=4e-2;
nlayers=36;

[d1,dN,N,delta,alpha]=layersUtility('firstLayerThickness',d1,'thickness',thick,'nSurfaceLayers',nlayers);

h4/dN
h5/dN
h6/dN
