clear all
close all
clc

% h0 is the base cell size
% h5 is the cell sizes at level 5
% d1 and thick are the desired first layer thickness and overall thicknes
% nlayers is the number of layers to be added

h0=1.0;

% case 1 (thin layer)

d1=0.001;
thick=0.1;
nlayers=18;

[d1,dN,N,delta,alpha]=layersUtility('firstLayerThickness',d1,'thickness',thick,'nSurfaceLayers',nlayers);

% case 2 (thick overall layer)

d1=0.001;
thick=1.0;
nlayers=113;

[d1,dN,N,delta,alpha]=layersUtility('firstLayerThickness',d1,'thickness',thick,'nSurfaceLayers',nlayers);

% case 3 (thick overall layer using relative sizes)

h5=h0/2^5;
d1=0.001;
thick=1.0;
nlayers=113;

[d1,dN,N,delta,alpha]=layersUtility('firstLayerThickness',d1,'thickness',thick,'nSurfaceLayers',nlayers);

% calculate the relative sizes

d1_rel=d1/h5
dN_rel=dN/h5
thick_ref=thick/h5
