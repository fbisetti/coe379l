clear all
close all
clc

% h0 is the base cell size
% h5 and h6 are level 5 and 6, respectively
% d1 and thick are the desired first layer thickness and overall thicknes
% nlayers is the number of layers to be added

h0=1.0+1.0/3.0;
h5=h0/2^5
h6=h0/2^6

d1=1.67e-4;
thick=4e-2;
nlayers=36;

% run the utility (must be in your path)
[d1,dN,N,delta,alpha]=layersUtility('firstLayerThickness',d1,'thickness',thick,'nSurfaceLayers',nlayers);

h6/dN
h5/dN
