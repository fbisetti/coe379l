function [d1,dN,N,delta,alpha]=layersUtility(varargin)


%
% function [d1,dN,N,delta,alpha]=layersUtility('PropertyName',PropertyValue)
%
% Helper function to design layers for use with the openFOAM utility
% snappyHexMesh
%
% --inputs--
%
% Property names are
%
%   'firstLayerThickness': thickness of first layer off the surface
%   'lastLayerThickness': thickness of last layer off the surface
%   'thickness': sum of thickness of all layers
%   'expansionRatio': ratio of layer thickness between two adjacent layers
%   'nSurfaceLayers': number of layers
%
%  User need to specify the property 'nSurfaceLayers' and two additional
%  properties among the following pairs:
%
%    firstLayerThickness AND expansionRatio
%    firstLayerThickness AND thickness
%    finalLayerThickness AND expansionRatio
%    finalLayerThickness AND thickness
%    thickness AND expansionRatio
%
%
% --outputs--
% d1: first layer thickness
% dN: last layer thickness
% N: number of layers
% delta: overall thickness
% alpha: expansion ratio (>1)
%

varargin;
if (~iseven(length(varargin)))
 error('fatal error. layersUtility requires input pairs');
end

% initialize
N=-1;
d1=-1;
delta=-1;
dN=-1;
alpha=-1;

% unpack inputs
for l=1:2:length(varargin)
   switch lower(varargin{l})
   case 'firstlayerthickness'
     d1=varargin{l+1};
   case 'thickness'
     delta=varargin{l+1}; 
   case 'finallayerthickness'
     dN=varargin{l+1}; 
   case 'expansionratio'
     alpha=varargin{l+1};
   case 'nsurfacelayers'
     N=varargin{l+1};
   otherwise
     varargin{l}
     error('fatal error. layersUtility does not recognize input property name');
   end 
end

% make sure the number of layers is given
if ( N==-1 )
  error('fatal error. layersUtility requires nSurfaceLayers as input property');
end

% figure out the conditions
if (d1~=-1)&(delta~=-1)&(dN==-1)&(alpha==-1)
  % find alpha by solving a non-linear eqn
  alpha=fzero(@fun1,1.5,[],delta/d1,N);
  dN=d1.*alpha.^(N-1);

elseif (d1~=-1)&(delta==-1)&(dN==-1)&(alpha~=-1)
  dN=d1.*alpha.^(N-1);
  delta=d1.*(alpha.^N-1)./(alpha-1);

elseif (d1==-1)&(delta==-1)&(dN~=-1)&(alpha~=-1)
  d1=dN./alpha.^(N-1);
  delta=dN.*(1.0/(alpha-1)).*(alpha.^N-1)./(alpha.^(N-1));

elseif (d1==-1)&(delta~=-1)&(dN~=-1)&(alpha==-1)
  % find alpha by solving a non-linear eqn
  alpha=fzero(@fun2,1.5,[],delta/dN,N);
  d1=dN./alpha.^(N-1);

elseif (d1==-1)&(delta~=-1)&(dN==-1)&(alpha~=-1)
  d1=delta./( (alpha.^N-1)./(alpha-1) );
  dN=delta./( (1.0/(alpha-1)).*(alpha.^N-1)./(alpha.^(N-1)) );

else

   fprintf('\n\n layersUtility requires nSurfaceLayers and one of the following input combinations\n');
   fprintf('\n firstLayerThickness AND expansionRatio -OR-\n'); 
   fprintf(' firstLayerThickness AND thickness -OR-\n');
   fprintf(' finalLayerThickness AND expansionRatio -OR-\n');
   fprintf(' finalLayerThickness AND thickness -OR-\n');
   fprintf(' thickness AND expansionRatio\n\n');

   error('fatal error.');

end

if ( alpha<1 )
 fprintf('\n\n layersUtility calculated expansion ratio < 1. This makes no sense and is probably due to a bad initial guess.\n\n');
 error('fatal error.'); 
end

% dump openFOAM commands for snappyHexMeshDict
fprintf('\n\n    nSurfaceLayers %d;',N);
fprintf('\n    firstLayerThickness %e;',d1);
fprintf('\n    finalLayerThickness %e;',dN);
fprintf('\n    thickness %e;',delta);
fprintf('\n    expansionRatio %e;\n\n',alpha);

% --- helper functions

return

% given delta/d1 & N, find alpha
function res=fun1(x,dd,N)
res=dd*(x-1)-(x.^N-1);
return

% given delta/dN & N, find alpha
function res=fun2(x,dd,N)
res=dd.*(x-1).*(x.^(N-1))-(x.^N-1);
return

