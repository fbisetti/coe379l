function [n,x,a,dn] = dlinear(l,d0,dn)

%
% function [n,x,a,dn] = dlinear(l,d0,dn)
%
% == inputs ==
% l  = length to be covered
% d0 = initial grid size
% dn = final grid size
%
% == outputs ==
% n  = number of points (including first and end points)
% x  = vector of x locations: x(1) = 0 & x(end) = l
% a  = max stretch rate
% dn = (actual) final grid size
%

if (dn<=d0)
  error('It must be dn > d0');
end

done = logical(0);
n    = round(l/sqrt(d0*dn));
while (~done)
  [x,a,dx_max,dx_min] = make_grid(l,n,d0);
  if (dx_max >= dn)
    n = n+1;
    [x,a,dn,dx_min] = make_grid(l,n,d0);
    done = logical(1);
  else
    n = n-1;
  end
end
