function [Fx,Fy,L,D,Mx,My,Mz]=wingLoads(geo,coeffs,air,U,aoa,croot,lplot)

%
% function [Fx,Fy,L,D,Mx,My,Mz]=wingLoads(geo,coeffs,air,U,aoa,croot,lplot) 
%
% Calculates the forces and moments on a 3D wing built extruding
% the root section in the spandwise direction. The sections along the span may be
% scaled and translated, but not rotated, according to a prescribed curve.
%
% The forces and moments are computed using aerodynamic coefficients for 2D airfoil
% sections that the user inputs.
%
% --inputs--
% geo: geometry structure with the following fields
%      s: array of parameter 0 <= s <=1, strictly ascending with s(1)=0 at the root and s(end)=1 at the tip
%      xs,ys,zs: coordinates (made dimensionless by the root chord) of leading edge of sections
%                the root section lies in the x-y plane with positive y identifying the upper section and the x-axis
%                corresponding to the chord line on the root section. The spanwise direction (extrusion direction) is z.
%      cs: scale factor for section at parameter value s, must be cs(1)=1 and 0< cs <=1
% coeffs: aerodynamic coefficients for the 2D airfoil section with the following fields
%         alpha: angle of attack vector
%         CL: lift coefficient vector
%         CD: drag coefficient vector
%         CM: pitching moment coefficient vector
%         so that CL(i), CD(i), CM(i) are the coefficients at alpha(i)
% air: air properties: air.T = temperature (C, degrees Celsius)
%                      air.p = pressure (atm)
% U: freestream velocity (m/s)
% aoa: angle of attack
% croot: chord of the root section (m)
% lplot: =1 plot figures, =0 do not
%
% --outputs--
%
% Fx: axial force (N)
% Fy: normal force (N)
% L: lift force (N)
% D: drag force (N)
% Mx,My,Mz: components of the moment vector (N-m) calculated with respect to the
%           leading edge on the root section (xs(1),ys(0),zs(1)) at s(1)=0.
%


% fontsize for plots
fs=14;
ms=10;
lw=3;

% extract
s=geo.s;
xs=geo.xs;
ys=geo.ys;
zs=geo.zs;
cs=geo.cs;

%
% run some checks for consistency
%
if ( (s(end)~=1)|(s(1)~=0)|(~issorted(s,'strictascend')) )
   error('wingLoads. fatal error. geo.s must be 0<=s<=1 and strictly ascending');
end
%
if (cs(1)~=1) error('wingLoads. fatal error. geo.cs(1) must be 1'); end;
if ( any(cs>1)|any(cs<=0) ) error('wingLoads. fatal error. all elements of geo.cs must be <=1 and >0'); end;
%
if ( length(xs)~=length(s) ) error('wingLoads. fatal error. xs must have the same length as s'); end; 
if ( length(ys)~=length(s) ) error('wingLoads. fatal error. ys must have the same length as s'); end; 
if ( length(zs)~=length(s) ) error('wingLoads. fatal error. zs must have the same length as s'); end; 
if ( length(cs)~=length(s) ) error('wingLoads. fatal error. cs must have the same length as s'); end; 

T_K=273.15+air.T; % C -> K
p_Pa=101325.0*air.p; % atm -> Pa

% --- air and flow properties
%
W=28.97; % molar mass air g/mol
%
rho=p_Pa/((8314/W)*T_K); % kg/m3 (@ 1 atm and 25 C)
%
mu0=1.7894e-5; % Pa-s
T0=273.11; % K
S=110.56; % K
%
mu=mu0*((T_K/T0)^1.5)*((T0+S)/(T_K+S)); % Pa-s (use Sutherland's law) 
nu=mu/rho;

q=0.5*rho*U^2; % Pa (dynamic pressure)
Re=U/nu;   % Reynolds number per unit chord length


fprintf('\n\n ------ Air and flow properties and reference variables -----\n');
fprintf(' Density (kg/m3): %e\n',rho);
fprintf(' Velocity (m/s): %e\n',U);
fprintf(' Velocity (mph): %e\n',U*2.237);
fprintf(' Root chord (m): %e\n',croot);
fprintf(' Dynamic viscosity (Pa-s): %e\n',mu);
fprintf(' Kinematic viscosity (m2/s): %e\n',nu);
fprintf(' Dynamic pressure (Pa): %e\n',q);
fprintf(' Reynolds number per unit chord: %e\n',Re);
fprintf(' Reynolds number at root section: %e\n',Re*croot);
fprintf(' Reynolds number ratio (root/tip): %e\n\n',cs(1)/cs(end));

% trigonometry
cosa=cos((aoa/180)*pi); % deg -> rad
sina=sin((aoa/180)*pi); % deg -> rad

% query aerodynamic coefficients
CL_=interp1(coeffs.alpha,coeffs.CL,aoa);
CD_=interp1(coeffs.alpha,coeffs.CD,aoa);
CM_=interp1(coeffs.alpha,coeffs.CM,aoa);

% normal and axial
CN_  =  CL_*cosa + CD_*sina;
CA_  = -CL_*sina + CD_*cosa;

fprintf('\n\n ------ Aerodynamic coefficients -----\n');
fprintf(' Angle of attack (degrees): %e\n',aoa);
fprintf(' CL : %e\n',CL_);
fprintf(' CD : %e\n',CD_);
fprintf(' CN : %e\n',CN_);
fprintf(' CA : %e\n',CA_);
fprintf(' CM @ 1/4 : %e\n\n',CM_);

fprintf('\n\n ------ Forces and moment per unit span on root section -----\n');
fprintf(' Root chord (m): %e\n',croot);
fprintf(' L'' (N/m): %e\n',CL_*q*croot);
fprintf(' D'' (N/m): %e\n',CD_*q*croot);
fprintf(' N'' (N/m): %e\n',CN_*q*croot);
fprintf(' A'' (N/m): %e\n',CA_*q*croot);
fprintf(' M'' @ 1/4 (N-m/m): %e\n\n',CM_*q*croot^2);

ctip=cs(end)*croot;

fprintf('\n\n ------ Forces and moment per unit span on tip section -----\n');
fprintf(' Tip chord (m): %e\n',ctip);
fprintf(' L'' (N/m): %e\n',CL_*q*ctip);
fprintf(' D'' (N/m): %e\n',CD_*q*ctip);
fprintf(' N'' (N/m): %e\n',CN_*q*ctip);
fprintf(' A'' (N/m): %e\n',CA_*q*ctip);
fprintf(' M'' @ 1/4 (N-m/m): %e\n\n',CM_*q*ctip^2);

if ( lplot )

  figure
  plot(geo.zs,geo.xs,'k.-',geo.zs,geo.xs+geo.cs,'r.-')
  legend('leading edge','trailing edge');
  axis equal
  xlabel('z coordinate'); ylabel('x coordinate');
  title('Wing plan view');
  set(gca,'FontSize',fs);
  
  figure
  plot(geo.s,geo.cs,'k.-')
  xlabel('curve parameter, s'); ylabel('chord scale factor, cs');
  title('Scale factor cs');
  set(gca,'FontSize',fs);

  figure; h=plot(coeffs.alpha,coeffs.CL,'k.-',aoa,CL_,'ro');
  title('CL - lift'); set(h(2),'MarkerSize',ms,'LineWidth',lw); 
  legend('input data','aoa');
  grid on
  xlabel('angle of attack');
  ylabel('lift coefficient (per unit span)');
  set(gca,'FontSize',fs);
  
  figure; h=plot(coeffs.alpha,coeffs.CD,'k.-',aoa,CD_,'ro');
  title('CD - drag'); set(h(2),'MarkerSize',ms,'LineWidth',lw); 
  legend('input data','aoa');
  grid on
  xlabel('angle of attack');
  ylabel('drag coefficient (per unit span)');
  set(gca,'FontSize',fs);
  
  figure; h=plot(coeffs.alpha,coeffs.CM,'k.-',aoa,CM_,'ro');
  title('CM - pitching moment'); set(h(2),'MarkerSize',ms,'LineWidth',lw);
  legend('input data','aoa');
  grid on
  xlabel('angle of attack');
  ylabel('moment coefficient (per unit span)');
  set(gca,'FontSize',fs);

  figure; h=plot(coeffs.alpha,coeffs.CL./coeffs.CD,'k.-',aoa,CL_./CD_,'ro');
  title('CL/CD - lift over drag'); set(h(2),'MarkerSize',ms,'LineWidth',lw); 
  legend('input data','aoa');
  grid on
  xlabel('angle of attack');
  ylabel('lift over drag, CL/CD');
  set(gca,'FontSize',fs);

end

% moment reference point
xyz_mom=[xs(1),ys(1),zs(1)]; % leading edge of the root chord

% now loop on all sections
for i=1:length(s)

   c=cs(i); % chord of section (dimensionless by root chord)

   % force per unit span

   dN_(i)=CN_*c; % normal (y)
   dA_(i)=CA_*c; % axial (x)
   dF_=[dA_(i),dN_(i),0]; % store in vector form (no z component of force)

   % moment per unit span

   % position of 1/4 chord of local section with respect to the reference point 
   r=[ xs(i)+c*0.25 , ys(i) , zs(i) ] - xyz_mom;

   % transform moment from 1/4 chord of local section to reference point
   dM_(i,:)=cross(r,dF_) + [0,0,-CM_*(c^2)]; % note negative sign on CM_ to comply with pitching moment and axis orientation 

end

if ( lplot )

  figure
  plot(geo.zs,dN_,'k.-',geo.zs,dA_,'r.-');
  legend('normal force','axial force');
  xlabel('z');
  ylabel('local force (-)');
  set(gca,'FontSize',fs);

  figure
  plot(geo.zs,dM_(:,1),'k.-',geo.zs,dM_(:,2),'r.-',geo.zs,dM_(:,3),'b.-');
  legend('Mx','My','Mz');
  xlabel('z');
  ylabel('local moment components (-)');
  set(gca,'FontSize',fs);

end

% now integrate over span (z) with trapezoidal
dz = zs(2:end)-zs(1:end-1);
%
N = sum( 0.5*(dN_(1:end-1)+dN_(2:end)).*dz );
A = sum( 0.5*(dA_(1:end-1)+dA_(2:end)).*dz );
%
for i=1:3 % component by component
  M(i) = sum( 0.5*(dM_(1:end-1,i)+dM_(2:end,i)).*(dz') );
end
%
area_planform = sum( 0.5*(cs(1:end-1)+cs(2:end)).*dz );

% lift and drag
L = N*cosa - A*sina; 
D = N*sina + A*cosa;

fprintf('\n\n ---- Summary of dimensional loads on actual 3D wing ----\n');
fprintf(' Planform area (m2) : %e\n',area_planform*(croot^2));
fprintf(' Root chord (m) : %e\n',croot);
fprintf(' Fx,axial (N) : %e\n',A*q*(croot^2));
fprintf(' Fy,normal (N) : %e\n',N*q*(croot^2));
fprintf(' Lift (N) : %e\n',L*q*(croot^2));
fprintf(' Drag (N) : %e\n',D*q*(croot^2));
fprintf(' Moment reference point (x,y,z) (m) : (%e,%e,%e)\n',xyz_mom*croot);
fprintf(' Moment vector @ reference point (Mx,My,Mz) (N-m) : (%e,%e,%e)\n\n',M*q*(croot^3));

% ---- outputs

% scale by root chord to the power of 2 (area)
Fx = A*q*(croot^2); 
Fy = N*q*(croot^2); 
L  = L*q*(croot^2); 
D  = D*q*(croot^2); 

% scale by root chord to the power of 3 (area x distance)
Mx = M(1)*q*(croot^3);
My = M(2)*q*(croot^3);
Mz = M(3)*q*(croot^3);

fprintf('\n\n Bye!\n\n'); 
