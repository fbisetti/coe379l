function mesher3d(model,N,a)

%
% function mesher3d(model,N,a)
%
% --inputs--
% model: one of 'cylinder' or 'sphere'
% N: number of steps in 1 decade drop of pressure.
% a: shape equivalent radius for scaling
%


switch lower(model)

  %--- do not change these unless you know what you are doing ---

  case 'cylinder'
  p=2;
  % deltas 0.5, 0.1, 0.01, 0.001
  xi_u=[1.4,3.2,10.0,31.6];
  xi_p=[1.8,4.4,14.1,44.7];

  case 'sphere'
  p=3;
  % deltas 0.5, 0.1, 0.01, 0.001
  xi_u=[1.3,2.2,4.6,10.0];
  xi_p=[1.5,2.7,5.8,12.6];

otherwise
  error('mesher3D. fatal error. model must be one of cylinder or sphere');
end

log10h=1.0/N;

% determine constant space in logspace up to the end of the pressure delta
xi = 10.^([0:(log10h/p):log10(xi_p(end))]);
xi_mid = (xi(1:end-1)+xi(2:end))*0.5;
xi_less = xi(1:end-1);

% linear spacing
dxi=xi(2:end)-xi(1:end-1);

% regions based on pressure 
R = length(xi_p);
% pick grid size at beginning of each region
xi_locs=[1.0,xi_p(1:end-1)];
dxi_locs=10.^interp1(log10(xi_less),log10(dxi),log10(xi_locs),'linear','extrap');

figure(1)
loglog(xi,xi.^(-p),'ko-');
xlabel('r/a'); ylabel('(a/r)^p');
v=axis;
for r=1:R
  hold on
  loglog([xi_locs(r),xi_locs(r)],v(3:4),'r--');
end
loglog([xi_p(end),xi_p(end)],v(3:4),'r--');
hold off
axis([0.5,v(2:end)]);

figure(2)
plot(xi_less,dxi,'k.-',xi_locs,dxi_locs,'ro');
xlabel('r/a'); ylabel('d(r/a)');
v=axis;
for r=1:R
  hold on
  plot([xi_locs(r),xi_locs(r)],v(3:4),'r--');
end
hold off
axis([0.5,v(2:end)]);

% levels
dxi_min=dxi_locs(1); % smallest mesh
factors=dxi_locs(2:end)./dxi_locs(1:end-1);
level_change=ceil(log2(factors));
levels=[0,cumsum(level_change)];

% actual
%dxi_min*2.^levels;
dxi_max=dxi_min*2^levels(end);
LL=levels(end);

fprintf('\n\n NOTE: unless otherwise noted, all dimensions are made dimensionless\n');
fprintf('       by the radius of the shape equivalent\n\n');

% write a recap
fprintf('\n\n ----- mesher 3D summary ----\n');
fprintf(' model: %s\n',model);
fprintf(' velocity exponent: %4.1f\n',-p);
fprintf(' # of regions %d\n',R);
fprintf(' max level L = %d\n',LL);
fprintf(' # of levels L+1 = %d\n',LL+1);

fprintf('\n\n ----- Region boundaries --------\n');
fprintf(' region    r(min)         r(max) \n');
for r=1:R-1
   fprintf('  %d       %e      %e\n',r,xi_locs(r),xi_locs(r+1));
end 
fprintf('  %d       %e      \n',R,xi_locs(R));
fprintf('\n\n');


fprintf('\n ----- Regions and levels --------\n');
fprintf(' region    level      cell size\n');
for r=1:R
   fprintf('  %d         %d          %e\n',R-r+1,LL-levels(end-r+1),dxi_min*2^levels(end-r+1));
end 
fprintf('\n\n');

L = 2.0*xi_p(3);
W = L;
A = xi_p(4)-L*0.5;
B = A;
C = A;
H = xi_p(4);

fprintf('\n ----- BlockMeshDict hints (in dimensional units) --------\n');
fprintf(' User defined radius equivalent : %6.2f\n',a);
fprintf(' h(min), approximate : %e\n',dxi_min*a);
fprintf(' h(max), approximate : %e\n',dxi_max*a);
fprintf(' A = B = C = %6.1f\n',ceil(A*a));
fprintf(' L = W = %6.1f\n',ceil(L*a));
fprintf(' H = %6.1f\n',ceil(H*a));
fprintf(' NxA = NxB = NyC = %d\n',ceil(ceil(A)/dxi_max));
fprintf(' NxL = NyW = %d\n',ceil(ceil(L)/dxi_max));
fprintf(' NzH = %d\n\n',ceil(ceil(H)/dxi_max));

fprintf('\n\n ----- snappyHexMeshDict hints (in dimensional units) --------\n');
fprintf(' sphere      radius   region level \n');
for r=1:R-1
   fprintf('  %d         %6.2f      %d\n',r,a*xi_locs(r+1),LL-levels(r));
end 

fprintf('\n\n Bye! \n\n\n');


