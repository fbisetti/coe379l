function [CD_lam,CD_lam_asympt,cf_lam,cf_lam_asympt,CD_turb,cf_turb,G] = flatPlateFrictionCoeff(Re,lturb)

%
% function [CD_lam,CD_lam_asympt,cf_lam,cf_lam_asympt,CD_turb,cf_turb,G] = flatPlateFrictionCoeff(Re,lturb)
%
% Friction coefficient (average and local) for flow on a smooth flat plate
% at zero inclination. Drag is on one side only.
%
% -- Average friction coefficient
% CD = D / (A q) where D is the drag force experienced by the plate of length L
% and planform area A, wetted on one side only, and q = rho U^2 / 2 is the dynamic
% pressure associated with the fluid of density rho and free stream velocity U
%
% -- Local friction coefficient (x=L)
% cf(x=L) = \tau_w / q where \tau_w is the wall shear stress at x=L
%
% NOTE. By definition, cf(x) = d( x CD(x) )/dx = x dCD/dx + CD
%
% -- inputs --
% Re: Reynolds number, Re = UL/nu
%     U: free stream velocity
%     L: plate length
%     nu: kinematic viscosity
%     if lturb is true, Re may *not* be a vector
% lturb: logical. If false, skip coefficients for turbulent flow
%
% --outputs--
% CD_lam: average friction coefficient for fully laminar plate (see Eq. (2.10), p. 33 in Ref. [1])
% CD_lam_asympt: average friction coefficient for fully laminar plate (see Eq. (14.62), p. 400 in Ref. [1])
% CD_turb: average friction coefficient for fully turbulent plate.
%                 See Section 18.2.5 (p. 582 in Ref. [1]), where CD may be computed
%                 solving Eq. (18.98) -or- combining Eq. (18.99) with the solution to Eq. (17.60) for G. 
% G: G function used in the calculation of CD for a fully turbulent plate obtained by 
%      solving Eq. (17.60), p. 537 in [1] with Lambda = ln(Re) and D = 2ln(kappa) + kappa(C+ - 3.0)
%
% cf_lam: local friction coefficient at x=L, obtained by taking the derivative of the average in x 
% cf_lam_asympt: friction coefficient at x=L, obtained by taking the derivative of the average in x 
% cf_turb: friction coefficient at x=L for a turbulent BL (see Eq. (18.96), p. 583 in Ref. [1]) 
%
% -- References --
% [1] - H. Schlichting and K. Gersten, Boundary-Layer Theory, 9th Edition, Springer, 2017 
%

if ( (lturb)&(length(Re)>1) ) error('flatPlateFrictionCoeff does not take vector inputs if lturb = logical(1)'); end;

% do not change...
kappa = 0.41;
Cplus = 5.0;

% -- average 
CD_lam = [];
CD_lam_asympt = [];
CD_turb = [];
G = [];

% -- local
cf_lam = [];
cf_lam_asympt = [];
cf_turb = [];

% -- average 
CD_lam = 1.328*Re.^(-0.5); 
CD_lam_asympt = 1.328*Re.^(-0.5) + 2.670*Re.^(-7.0/8.0); 

if ( lturb )
  opts = optimset('TolX',1*1e-7);
  % -- average turbulent
  D = 2*log(kappa) + kappa*(Cplus-3.0);
  lambda = log(Re);
  G = fzero(@myrhs1,1,opts,lambda,D);
  CD_turb = 2.0 * ( (kappa./lambda) * G ).^2;
  %CD_turb_2 = fzero(@myrhs2,CD_turb,[],kappa,Cplus,Re);
  %[CD_turb,CD_turb_2]
end

% --- local laminar coefficients at x=L
% obtained as cf = x dCD/dx + CD
cf_lam = 1.328*(-0.5)*Re.^(-0.5) + CD_lam;
cf_lam_asympt = 1.328*(-0.5)*Re.^(-0.5) + 2.670*(-7.0/8.0)*Re.^(-7.0/8.0) + CD_lam_asympt;

if ( lturb )
  % --- local turbulent coefficient at x=L
  cf_turb = 2.0*(sqrt(2.0/CD_turb) + 1.0/kappa)^(-2.0);
  % solve Eq. (18.96)
  opts = optimset('TolX',cf_turb*1e-7);
  cf_turb = fzero(@myrhs3,cf_turb,opts,kappa,CD_turb);
end

return

% Eq. (17.60) 
function y = myrhs1(x,lambda,D)
y=lambda./x + 2.0*log(lambda./x)-D-lambda;

% Eq. (18.98) 
function y = myrhs2(x,kappa,Cplus,Re)
y = sqrt(2.0./x) - ( (1.0/kappa)*log(0.5*x*Re) + (Cplus-3.0) );

% Eq. (18.96) -- without approximations
function y = myrhs3(x,kappa,CD)
y = CD-x*(1.0+(2.0/kappa)*sqrt(x/2.0));

