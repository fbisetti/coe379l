function y = geomseries(a,n)

%
% function y = geomseries(a,n)
%
% y = 1 + a + a^1 + ... + a^n,  n >= 0
%

if ( n < 0 )
  error('n must be >= 0');
end

y = sum(a.^[0:n]);
