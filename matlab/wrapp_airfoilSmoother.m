clear all
close all
clc

fin='CLARKY.dat';
opts.fout='CLARKY_R5.5E-3.csv';
opts.radius=5.5e-03;
opts.xlead=2.0e-02;
opts.h=4.1e-03;
opts.closeairfoil=1;
opts.opencurve=0;
opts.rescale2unitchord=1;
opts.shift2quarterchord=0;
opts.commaseparated=0;
opts.onShape=0;
opts.noheader=0;
opts.addzerozeta=1;
opts.fpdf='CLARKY.pdf';
opts.pdffull=1;

airfoilSmoother(fin,opts);


