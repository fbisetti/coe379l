function airfoilSmoother(fin,opts)

%
% function airfoilSmoother(fin,opts)
%
% Takes airfoil geometry in Selig format (e.g. downloaded from
% http://www.airfoiltools.com/) and prepares it to be imported
% into a CAD program for further use in building 3D wing geometry for simulations
% and 3D printing. Also, it outputs various other parameters.
%
% In the process, it uses splines to increase the point resolution along
% the upper and lower surfaces and possibly adds a closed curve at the trailing
% edge with a user-defined radius of curvature.
%
% --inputs--
% fin: text file of the aifoil coordinates in Selig format
% opts: options structure (see below for details)
%       with the following fields
%       
%       fout:   name of output file
%       radius: radius of curvature at trailing edge (added if closeairfoil=1)
%       xlead:  approximate extend of the leading edge region along chord line
%       h:      spacing for smoothed airfoil geometry
%       closeairfoil: if =1, close the airfoil's trailing edge with the prescribed radius of curvature
%       opencurve:    if =0, x(1)=x(end) and y(1)=y(end)
%                     if =1, do not duplicate the first and last points (x,y)
%       rescale2unitchord:  if =1, after closing the airfoil, the chord may not be equal to unity.
%                               if true, rescale it
%                           if =0, do not rescale
%       shift2quarterchord: if =1, shift the x coordinate so that x=0 corresponds to the quarter chord location
%       commaseparated:  =1, use commas to separate coordinates, rather than spaces (=0), producing a CSV file
%       onShape:         if =1, make sure output file is ready to be imported into onShape
%       noheader:        if =1, skip header to the file
%       addzerozeta:     if =1, add a third column of zeros, representing the z coordinate
%       fpdf:            if not empty, produce a PDF file of the airfoil and save it with name fpdf
%       pdffull:         if =1, add chord line and quarter chord location on the PDF
%

fprintf('\n\n Importing airfoil from %s\n',fin);

[HH,Q]=readdata(fin);

xi=Q(:,1); [xmax,ixmax]=max(xi); [xmin,ixmin]=min(xi);
yi=Q(:,2); [ymax,iymax]=max(yi); [ymin,iymin]=min(yi);

n=size(xi,1);

fprintf('\n ---- airfoil details ----\n\n');
fprintf(' # points = %d\n\n',n);
fprintf(' xmin(i=%3d) = %e\n',ixmin,xmin);
fprintf(' xmax(i=%3d) = %e\n',ixmax,xmax);
fprintf(' ymin(i=%3d) = %e\n',iymin,ymin);
fprintf(' ymax(i=%3d) = %e\n\n',iymax,ymax);

fprintf(' y @ xmin = %e\n',yi(ixmin));
fprintf(' y @ xmax = %e\n',yi(ixmax));
fprintf(' x @ ymin = %e\n',xi(iymin));
fprintf(' x @ ymax = %e\n\n',xi(iymax));

% split it in top and bottom

%
% assumes SELIG format, starting at trailing edge and moving to leading edge
%

for i=2:n
 if ( (yi(i)<0) & ((xi(i)-xi(i-1))>0) )
   ilead=i-1;
   break;
 end
end

idx_top=logical(ones(size(xi)));
idx_top(ilead+1:end)=logical(0);

idx_bot=logical(zeros(size(xi)));
idx_bot(ilead:end)=logical(1);

% top
xi_top = xi(idx_top);
yi_top = yi(idx_top);

% bottom
xi_bot = xi(idx_bot);
yi_bot = yi(idx_bot);

% plot it split
figure
h=plot(xi_top,yi_top,'ro-',xi_bot,yi_bot,'bo-',[xmin,xmax],[yi(ixmin),yi(ixmin)],'k-');
legend('Upper surface','Lower surface','chord line');
xlabel('x'); ylabel('y');
set(h(1),'MarkerSize',6,'MarkerFaceColor','red');
set(h(2),'MarkerSize',12);
title(sprintf('Data from file %s',fin),'Interpreter', 'none');

% leading edge top
idx=xi_top<=opts.xlead;
yi_lead_top = yi_top(idx); [yi_lead_top,jj]=sort(yi_lead_top,'ascend');
xi_lead_top = xi_top(idx); xi_lead_top = xi_lead_top(jj); 

% fit curve x=x(y) with constraint that at (x,y)=(0,0) one has x'(y) = 0
pp_lead_top = csape(yi_lead_top,[0 xi_lead_top' xi_lead_top(end)],[1 0]);
pp_lead_top_der = fnder(pp_lead_top);
dydx_top_P=1.0/ppval(pp_lead_top_der,yi_lead_top(end));
x_top_P=xi_lead_top(end);
y_top_P=yi_lead_top(end);

% leading edge bottom
idx=xi_bot<=opts.xlead;
yi_lead_bot = yi_bot(idx); [yi_lead_bot,jj]=sort(yi_lead_bot,'ascend'); 
xi_lead_bot = xi_bot(idx); xi_lead_bot = xi_lead_bot(jj); 

% fit curve x=x(y) with constraint that at (x,y)=(0,0) one has x'(y) = 0
pp_lead_bot = csape(yi_lead_bot,[xi_lead_bot(1) xi_lead_bot' 0],[0 1]);
pp_lead_bot_der = fnder(pp_lead_bot);
dydx_bot_Q=1.0/ppval(pp_lead_bot_der,yi_lead_bot(1));
x_bot_Q=xi_lead_bot(1);
y_bot_Q=yi_lead_bot(1);

% now fit the top towards the trailing edge
idx=xi_top>=x_top_P;
yi_trail_top = yi_top(idx); [yi_trail_top,jj]=sort(yi_trail_top,'ascend');
xi_trail_top = xi_top(idx); xi_trail_top = xi_trail_top(jj); 

% fit curve x=x(y) with constraint that at (x,y)=(0,0) one has x'(y) = 0
pp_trail_top = csape(xi_trail_top,[dydx_top_P yi_trail_top' yi_trail_top(end)],[1 0]);

% now fit the bottom towards the trailing edge
idx=xi_bot>=x_bot_Q;
yi_trail_bot = yi_bot(idx); [yi_trail_bot,jj]=sort(yi_trail_bot,'ascend');
xi_trail_bot = xi_bot(idx); xi_trail_bot = xi_trail_bot(jj); 

% fit curve x=x(y) with constraint that at (x,y)=(0,0) one has x'(y) = 0
pp_trail_bot = csape(xi_trail_bot,[dydx_bot_Q yi_trail_bot' yi_trail_bot(end)],[1 0]);

% now build a table in SELIG format
% using the splines using h as increment
h=opts.h;

% start from the trailing edge

% trail top
xj_=[[1.0:-h:x_top_P],x_top_P]; xj_=sort(unique(xj_),'descend');;
yj_=ppval(xj_,pp_trail_top);
xj=xj_(1:end-1); yj=yj_(1:end-1);

% lead top
yj_=[[y_top_P:-h:0],0]; yj_=sort(unique(yj_),'descend');
xj_=ppval(yj_,pp_lead_top);
xj=[xj,xj_(1:end-1)]; yj=[yj,yj_(1:end-1)];

% lead bottom
yj_=[[0:-h:y_bot_Q],y_bot_Q]; yj_=sort(unique(yj_),'descend');
xj_=ppval(yj_,pp_lead_bot);
xj=[xj,xj_(1:end-1)]; yj=[yj,yj_(1:end-1)];

% trail bottom
xj_=[[x_bot_Q:h:1],1]; xj_=sort(unique(xj_),'ascend');
yj_=ppval(xj_,pp_trail_bot);
xj=[xj,xj_(2:end)]; yj=[yj,yj_(2:end)];


% find the last points at the trailing edge

if ( opts.closeairfoil )

  R=opts.radius;
  
  done=logical(0); i=1;
  while (~done)
  
    xAB=xj(i:i+1);
    yAB=yj(i:i+1);
    %
    xCD=xj(end-i:end-i+1); xCD=xCD(end:-1:1);
    yCD=yj(end-i:end-i+1); yCD=yCD(end:-1:1);
  
    theta=angle( complex(xAB(2)-xAB(1),yAB(2)-yAB(1)) );
    lAB=sqrt( (xAB(2)-xAB(1))^2 + (yAB(2)-yAB(1))^2 );
  
    psi=angle( complex(xCD(2)-xCD(1),yCD(2)-yCD(1)) );
    lCD=sqrt( (xCD(2)-xCD(1))^2 + (yCD(2)-yCD(1))^2 );
  
    M = [ cos(theta), -cos(psi);
          sin(theta), -sin(psi) ];
    %
    B(1,1) = -xAB(1) -R*cos(theta+pi*0.5) + xCD(1) + R*cos(psi-pi*0.5);
    B(2,1) = -yAB(1) -R*sin(theta+pi*0.5) + yCD(1) + R*sin(psi-pi*0.5);
  
    q = M\B;
    s = q(1);
    r = q(2);
  
    if (s<=lAB)&(r<=lCD)
     
      %[theta*180/pi,s,lAB]
      %[psi*180/pi,r,lCD]
  
      xP = xAB(1) + s*cos(theta);
      yP = yAB(1) + s*sin(theta);
      %
      xQ = xCD(1) + r*cos(psi);
      yQ = yCD(1) + r*sin(psi);
      %
      xW = xAB(1) + s*cos(theta) + R*cos(theta+pi*0.5);
      yW = yAB(1) + s*sin(theta) + R*sin(theta+pi*0.5);
  
      done=logical(1); 
  
    else
      i=i+1;
    end
  
    if ( xAB(1)<0.75 )
      error('fatal error: airfoilSmoother: passed 3/4 chord looking for location to smooth trailing edge. something fishy going on.');
    end
  
  end
  
  % angles
  thetaP=angle( complex(xP-xW,yP-yW) );
  thetaQ=angle( complex(xQ-xW,yQ-yW) );
  theta_bot=linspace(thetaQ,0,10);
  theta_top=linspace(0,thetaP,10);
  xTE_top=xW+R*cos(theta_top);
  yTE_top=yW+R*sin(theta_top);
  xTE_bot=xW+R*cos(theta_bot);
  yTE_bot=yW+R*sin(theta_bot);
  
  figure
  h=plot(xAB,yAB,'ro-',xCD,yCD,'bo-',xj,yj,'k--',...
         xW,yW,'ko',xP,yP,'r*',xQ,yQ,'bs',...
         xTE_top,yTE_top,'r-',xTE_bot,yTE_bot,'b-');
  xlabel('x'); ylabel('y');
  set(h(1),'MarkerSize',6,'MarkerFaceColor','red');
  set(h(2),'MarkerSize',12);
  title(sprintf('Detail of the trailing edge (file %s)',fin),'Interpreter', 'none');
  axis equal;
  %
  aa=[min([xAB,xCD])-2*R,max([xAB,xCD])+2.0*R,min([yAB,yCD])-2.0*R,max([yAB,yCD])+2.0*R];
  axis(aa);
  
  % now splice...
  done=logical(0); itop=1;
  while ~done
    if ( xj(itop)<min(xTE_top) )
       done=logical(1);
    else
       itop=itop+1;
    end
  end
  
  done=logical(0); ibot=0;
  while ~done
    if ( xj(end-ibot)<min(xTE_bot) )
       done=logical(1);
    else
       ibot=ibot+1;
    end
  end
  
  % patch
  xj=[xTE_top,xj(itop:end-ibot),xTE_bot];
  yj=[yTE_top,yj(itop:end-ibot),yTE_bot];

  % write a recap
  fprintf('\n Closed trailing edge with user-defined radius of curvature %e\n',opts.radius);
  fprintf(' Airfoil trailing edge is now at (x,y) = (%e,%e)\n',xW+R,yW);
  fprintf(' Airfoil is %5.3f%% shorter\n\n',(1.0-xW+R)*100);

  if ( opts.opencurve )
    fprintf('\n > removing repeated point at trailing edge to obtain an open curve\n');
    xj(end)=[];
    yj(end)=[];
  end

end

% rescale to unit chord
xs=0;
if ( opts.rescale2unitchord )
 fprintf('\n > rescaled chord to unity\n');
 L=max(xj)-min(xj); 
 xj=xj./L; yj=yj./L; % scale
 %xj=xj-min(xj); % shift to leading edge 
end

% shift
xs=0;
if ( opts.shift2quarterchord )
 fprintf('\n > shifted origin to quarter chord\n\n'); 
  xs=-0.25;
end

s='';
if ( opts.commaseparated )
  s=',';
end

pat3='%23.16e %s %23.16e %s %23.16e\n';
pat2='%23.16e %s %23.16e\n';
if ( opts.onShape )
  pat3='%23.16f %s %23.16f %s %23.16f\n';
  pat2='%f %s %f\n';
end

fprintf('\n Writing improved airfoil (h = %e, N = %d) to %s\n\n',opts.h,length(xj),opts.fout);

% now dump them in a file
fid=fopen(opts.fout,'w');
if ( (~opts.noheader)&(~opts.onShape) ) fprintf(fid,'# %s (h = %e, N = %d)\n',HH{1},opts.h,length(xj)); end;

if ( opts.addzerozeta )
  for j=1:length(xj)
    fprintf(fid,pat3,xj(j)+xs,s,yj(j),s,0.0);
  end
else
  for j=1:length(xj)
    fprintf(fid,pat2,xj(j)+xs,s,yj(j));
  end
end
fclose(fid);

figure
plot(xi+xs,yi,'ko-',xj+xs,yj,'r.-');
xlabel('x'); ylabel('y');
title(sprintf('Data output to file %s',opts.fout),'Interpreter', 'none');
axis equal
hold on
v=axis;
plot(v(1:2),[0,0],'k-',[0.25+xs,0.25+xs],v(3:4),'k-');
legend('Original data','Improved data','Chord line','quarter chord normal');
hold off

if ( ~isempty(opts.fpdf) )
  figure
  h=plot(xj+xs,yj,'k-');
  xlabel('x'); ylabel('y');
  title(sprintf('Airfoil (file: %s)',opts.fout),'Interpreter', 'none');
  set(h(1),'Linewidth',2,'MarkerFaceColor','white');
  axis equal
  v=axis;
  thick = max(yi)-min(yi);
  axis([v(1)-0.5,v(2)+0.5,v(3)-2*thick,v(4)+2*thick]);
  if ( opts.pdffull )
    hold on
    h=plot([v(1)-0.25,v(2)+0.25],[0,0],'k-',0.25+xs,0,'ko');
    set(h(2),'MarkerSize',6,'Linewidth',2);
    hold off
  end
  gcf; print(opts.fpdf,'-dpdf');
end


