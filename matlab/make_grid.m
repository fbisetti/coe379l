function [x,a,dx_max,dx_min] = make_grid(L,n,dx)

%
% function [x,a,dx_max,dx_min] = make_grid(L,n,dx)
%
%    x=0                       x=L
%     |-----|---- ... ----|-----|
%     1     2            n-1    n
%       dx
%
%     L  = length
%     n  = number of points (including extremes)
%     dx = initial grid size
%
%     x      = array of x(i), 0 <= x <= L
%              [ 0, dx,  dx*(1+a), dx(1+a+a^2), ..., dx*(1+..+a^(n-2)) ]
%     a      = stretch factor
%     dx_max = max value of dx 
%     dx_min = min value of dx 
%

rtol = 1e-11;
opts = optimset('TolX',L*rtol);

r = L/dx;
a = fzero(@lrhs,1.0,opts,n,r);

x = zeros(1,n);
for i = 2:n
  x(i) = x(i-1) + dx*a^(i-2);
end

dx_max = max(x(2:end)-x(1:end-1));
dx_min = min(x(2:end)-x(1:end-1));

function y = lrhs(a,n,r)

y = geomseries(a,n-2)-r;



