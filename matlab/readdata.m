function [hdr,A]=readdata(fin)

%
% function [hdr,A]=readdata(fin)
%
% Read a file containing numerical data in table format and
% possibly a header. The numerical entries must be
% separated by spaces and conform to a table.
%
% fin: name of file to read
% hdr: header (if present in the file)
% A: numerical data in table format
%

hdr = [];
A   = [];

% bail out if not there
if ( ~exist(fin,'file') )
  warning(sprintf('File %s not found. Returning with empty output.\n',fin));
  return;
end

txt=textread(fin,'%s','delimiter','\n','bufsize',4095*100);
n=length(txt); ld=1; lh=1;
for i=1:n
  line=txt{i}; line=strtrim(line);
  if (line(1)~='#')
    A(ld,:)=str2num(line);ld=ld+1;
  else
    hdr{lh}=txt{i}; lh=lh+1;
  end
end
