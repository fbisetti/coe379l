clear all
close all
clc

% specifications of wing shape
theta1=38*pi/180;
theta2=26*pi/180;
%
sizeP=0.635; % outer root section scale factor
sizeQ=0.2; % tip section scale factor
%
zQ=2.2; % wing span, top view

U=70/2.237; % wind speed, mph -> m/s
air.T=25; % air temperature, C, degree Celsius
air.p=1; % air pressure, atm
croot=5.5*2.54e-2; % root chord, m
aoa=10; % angle of attack, degrees

fin_coeffs='BACXXX_coefficients_Re2E5_Ncrit9.dat';
%fin_coeffs='BACXXX_coefficients_Re5E4_Ncrit9.dat';

lplot=1;

% --- compute leading edge curve

s=linspace(0,1,1000);

xP=1-sizeP;
zP=xP/tan(theta1);
xQ=xP+tan(theta2)*(zQ-zP);

xs=zeros(size(s));
ys=zeros(size(s));
zs=zQ*s;
cs=zeros(size(s));
%
idx=zs<=zP;
xs(idx)=(xP/zP)*zs(idx);
cs(idx)=1-((1-sizeP)/zP)*zs(idx);
%
idx=zs>=zP;
xs(idx)=xP+((xQ-xP)/(zQ-zP))*(zs(idx)-zP);
cs(idx)=sizeP+((sizeQ-sizeP)/(zQ-zP))*(zs(idx)-zP);

% pack the leading edge curve 
geo.s=s;
geo.xs=xs;
geo.ys=ys;
geo.zs=zs;
geo.cs=cs;

% read aerodynamic coefficients from file
fprintf('\nReading coefficients from %s\n\n',fin_coeffs);
[~,Q]=readdata(fin_coeffs);
coeffs.alpha=Q(:,1);
coeffs.CL=Q(:,2);
coeffs.CD=Q(:,3);
coeffs.CM=Q(:,5);

% compute
[Fx,Fy,L,D,Mx,My,Mz]=wingLoads(geo,coeffs,air,U,aoa,croot,lplot);


