clear all
close all
clc

l=1; % size of grid, x(end)-x(1)
d0=0.01; % first increment, x(2)-x(1)
dn=0.1; % final increment, x(end)-x(end-1)

[n,x,a,dn] = dlinear(l,d0,dn);

% n: number of points
% x: array
% a: constant stretch factor so that (x(i+1)-x(i))/(x(i)-x(i-1)) = a
% dn: final increment (recomputed to ensure an integer number of points)

dx=x(2:end)-x(1:end-1);
xm=(x(2:end)+x(1:end-1))*0.5; % mid points

figure(1); plot([1:n],x,'ko-'); xlabel('index, i'); ylabel('Grid point, x(i)');
figure(2); plot(xm,dx,'ko-'); xlabel('x(i)'); ylabel('increment dx(i)');

fprintf('\n\n Stretch rate is %6.4f\n',a);
fprintf(' Ratio dx(end)/dx(1) is %e\n',dx(end)/dx(1));
fprintf(' Nr cells is %d\n',length(x)-1);
fprintf(' Nr points is %d\n\n',length(x));


